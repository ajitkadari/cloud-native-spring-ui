package io.pivotal.cloudnativespringui;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import io.pivotal.cloudnativespring.domain.City;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;

@SpringUI
@Theme("valo")
public class AppUI extends UI {

    private final CloudNativeSpringUiApplication.CityClient cityClient;

    private final Grid<City> grid;

    @Autowired
    public AppUI(CloudNativeSpringUiApplication.CityClient client) {
        this.cityClient = client;
        grid = new Grid<>(City.class);
    }

    @Override
    protected void init(VaadinRequest request) {
        setContent(grid);
        grid.setWidth(100, Unit.PERCENTAGE);
        grid.setHeight(100, Unit.PERCENTAGE);
        Collection<City> collection = new ArrayList<>();
        cityClient.getCities().forEach(collection::add);
        grid.setItems(collection);
    }
}
